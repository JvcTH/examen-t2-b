using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MegamanController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    public GameObject rightBullet;
    public GameObject leftBullet;
    public GameObject rightBulletCharge;
    public GameObject leftBulletCharge;
    public GameObject rightBulletFullCharge;
    public GameObject leftBulletFullCharge;
    public float charge = 0f;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        animator.SetInteger("Estado", 0);

        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(6, rb.velocity.y); 
            sr.flipX = false;
            animator.SetInteger("Estado",1); 
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-6, rb.velocity.y); 
            sr.flipX = true; 
            animator.SetInteger("Estado",1); 
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * 40, ForceMode2D.Impulse);
            animator.SetInteger("Estado",2); 
        }   
        if (Input.GetKey(KeyCode.X)){
            charge += Time.deltaTime;
            
            if (sr.color == Color.green)
            {
                sr.color= Color.white;
            }
            sr.color= Color.green;
        }
        if (Input.GetKeyUp(KeyCode.X))
        {
            animator.SetInteger("Estado",3); 
            if (sr.color == Color.green)
            {
                sr.color= Color.white;
            }
            if (charge >= 5)
            {
                var bullet = sr.flipX ? leftBulletFullCharge : rightBulletFullCharge;
                var position = new Vector2(transform.position.x, transform.position.y);
                var rotation = rightBulletFullCharge.transform.rotation;
                Instantiate(bullet, position, rotation);
            }else
            {
                if (charge >= 3)
                {
                    var bullet = sr.flipX ? leftBulletCharge : rightBulletCharge;
                    var position = new Vector2(transform.position.x, transform.position.y);
                    var rotation = rightBulletCharge.transform.rotation;
                    Instantiate(bullet, position, rotation);
                }else
                {
                    var bullet = sr.flipX ? leftBullet : rightBullet;
                    var position = new Vector2(transform.position.x, transform.position.y);
                    var rotation = rightBullet.transform.rotation;
                    Instantiate(bullet, position, rotation);
                }
            }
            
            
            
            charge = 0;
            
        }
    }
}
