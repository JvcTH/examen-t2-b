using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DinoController : MonoBehaviour
{
    public int lifes = 10;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(lifes <= 0){
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Bullet"))
        {
            lifes = lifes - 2;
            
        }
        if (other.gameObject.CompareTag("BulletChage"))
        {
            lifes = lifes - 5;
            
        }
        if (other.gameObject.CompareTag("BulletFullChage"))
        {
            lifes = lifes - 10;
            
        }
    }
}
